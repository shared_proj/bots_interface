CREATE TABLE bots (
	id varchar2(255) PRIMARY KEY,
	version float,
	os_version varchar2(255),
	cpu varchar2(255),
	memory float,
	gpu varchar2(255),
	first_seen date DEFAULT CURRENT_TIMESTAMP,
	last_seen date,
	uptime date,
	last_ip varchar2(16)
);

CREATE TABLE results(
	id INT PRIMARY KEY,
	bot_id text,
	task text,
	result text,
	timestamp date DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (bot_id) REFERENCES bots(id)
);
