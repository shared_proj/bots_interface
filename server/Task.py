


class Task():
    def __init__(self, id, params):
        self.id = id
        self.params = params

    def toJSON(self):
        return {
            'id': self.id,
            'params': self.params
        }

