'use strict;';
        var app = angular.module('botsApp', ['ui.bootstrap']);

        app.controller('botsController', function ($scope) {
            $scope.modelData = {};
            $scope.headers = {
                'ID': 'id',
                'Version': 'version',
                'OS': 'os_version',
                'CPU': 'cpu',
                'Memory': 'memory',
                'GPU': 'gpu',
                'First seen': 'first_seen',
                'Last seen': 'last_seen',
                'Uptime': 'uptime',
                'Last IP': 'last_ip',
                'Country': 'country'
            };
            $scope.modelData.reverseSort = false;

            $scope.onlineBots = 0;
            $scope.bots = [];

        });