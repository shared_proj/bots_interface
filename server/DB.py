from flask import Flask
import pygeoip
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, render_template, session, request, g
from flask.ext.socketio import SocketIO, emit
from flask.ext.triangle import Triangle
from threading import Thread, Lock
from time import sleep, time
from Constants import constants
import json
import requests
from random import randint
import Queue


class DB():
    def __init__(self, dbfile):
        self.dbfile = dbfile


    def validLogin(self, user, passwd):
        """
        Check if the credentials exists
        :param user: username
        :param passwd: password
        :return: True if exists else false
        """
        db = self.get_db()
        db.row_factory = self.dict_factory
        try:
            cur = db.execute('SELECT username,password FROM admins WHERE username=? AND password=?', (user, passwd,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
        # RequestErr("SQL Error: {}".format(e.message),redir=False)
        finally:
            val = cur.fetchone()
            return True if val != None and len(val) > 0 else False


    def addBot(self, bot):
        """
        Adds bot information to the database
        :param bot: bot object

        """
        db = self.get_db()
        try:
            cur = db.execute('INSERT INTO bots VALUES (?,?,?,?,?,?,?,?,?,?)', (
                bot.id, bot.version, bot.os_version, bot.cpu, bot.memory, bot.gpu, bot.first_seen, bot.last_seen,
                bot.uptime, bot.last_ip,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        db.commit()

    def isNewBot(self, bot):
        """
        Check if a bot exists in the database

        :param bot: bot object
        :return: True if not exists else False
        """
        db = self.get_db()
        db.row_factory = self.dict_factory
        try:
            cur = db.execute('SELECT id FROM bots WHERE id=?', (bot.id,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        return len(cur.fetchall()) == 0

    def updateLastSeen(self, bot):
        """
        Update a bot last seen value
        :param bot: bot object

        """
        db = self.get_db()
        try:
            cur = db.execute('UPDATE bots SET last_seen=? WHERE id=?', (bot.last_seen, bot.id,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        db.commit()

    def getFirstSeen(self, bot):
        """
        Get the time that the bot was first seen by the server
        :param bot: bot object
        :return: The timestamp the bot was first seen by the server
        """
        db = self.get_db()
        db.row_factory = self.dict_factory
        try:
            cur = db.execute('SELECT first_seen FROM bots WHERE id=?', (bot.id,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        return cur.fetchone()

    def getBots(self, limit):
        """
        Get parital bot list from the database

        :param limit:
        :return: parital bot list from the database
        """
        db = self.get_db()
        db.row_factory = self.dict_factory
        try:
            cur = db.execute('SELECT * FROM bots LIMIT {} OFFSET ?'.format(constants.NUM_OF_RESULTS), (limit,))
            # cur = db.execute('SELECT * FROM bots LIMIT {} OFFSET ?'.format(constants.NUM_OF_RESULTS), (10,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        return cur.fetchall()

    def addTaskResult(self, bot, task, result):
        """
        Adds task result to the database
        :param bot: bot object
        :param task: task id
        :param result: result as string

        """
        db = self.get_db()
        try:
            cur = db.execute('INSERT INTO results(bot_id,task,result) VALUES (?,?,?)', (bot.id, task, result))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        db.commit()

    def getResults(self, offset):
        """
        Get parital list of task results from the database

        """
        db = self.get_db()
        db.row_factory = self.dict_factory
        try:
            cur = db.execute('SELECT * FROM results LIMIT ?', (constants.NUM_OF_RESULTS,))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
            # RequestErr("SQL Error: {}".format(e.message),redir=False)
        return cur.fetchall()

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def connect_db(self, dbpath):
        """Connects to the specific database."""
        rv = sqlite3.connect(dbpath)
        rv.row_factory = sqlite3.Row
        return rv

    def init_db(self):
        """Initializes the database."""
        db = self.get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
            db.commit()

    # @app.cli.command('initdb')
    def initdb_command(self):
        """Creates the database tables."""
        self.init_db()
        print('Initialized the database.')

    def get_db(self):
        """Opens a new database connection if there is none yet for the
        current application context.
        """
        # if not hasattr(g, 'sqlite_db'):
        if hasattr(g, 'sqlite_db'): self.close_db(g.sqlite_db)
        try:
            g.sqlite_db = self.connect_db('{}/{}'.format(constants.root_path, self.dbfile))
        except sqlite3.OperationalError as e:
            raise e

        return g.sqlite_db

    def close_db(self, error):
        """Closes the database again at the end of the request."""
        if hasattr(g, 'sqlite_db'):
            g.sqlite_db.close()

    ################################################################################################
#need to add some mehaniscim to filter
    def getlastOfflineBot(self):
        '''get th last offline bots'''
        db = self.get_db()
        db.row_factory = self.dict_factory
        currenttime=str(int(time()));
        try:
            cur = db.execute(('SELECT * FROM bots where {}-last_seen>30 and {}-last_seen<60  ORDER BY last_seen DESC ;'.format(currenttime,currenttime)))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
        return cur.fetchall()
        pass
    def getOfflineBots(self):
        '''get all offline bots from every time'''
        db = self.get_db()
        db.row_factory = self.dict_factory
        try:
            cur = db.execute(('SELECT * FROM bots where {}-last_seen>60 ORDER BY last_seen DESC ;'.format(str(int(time())))))
        except sqlite3.Error as e:
            print "SQL Error: {}".format(e.message)
        return cur.fetchall()

        ################################################################################################
